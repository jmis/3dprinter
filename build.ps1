param(
	$inputScadFiles = @(),
	$inputStlFiles = @(),
	[switch] $CompileEverything = $false
);

Write-Host "Cleaning build directory...";

Remove-Item "./build" -Recurse -Force
New-Item "./build/stl/" -type Directory;
New-Item "./build/gcode/" -type Directory;

if ($CompileEverything) {
	$scadFiles = Get-ChildItem -Path *.scad -r | Where-Object { $_.FullName -inotmatch "common" };
	$stlFiles = Get-ChildItem -Path *.stl -r
}
else {
	if ($inputScadFiles.Count -gt 0) { $scadFiles = Get-ChildItem $inputScadFiles; }
	if ($inputStlFiles.Count -gt 0) { $stlFiles = Get-ChildItem $inputStlFiles; }
}

if ($scadFiles.Count -gt 0) {
	Write-Host "Compiling SCAD files...";
	
	foreach ($scadFile in $scadFiles) {
		Write-Host "`t" $scadFile.Name;
		$outFileName = $scadFile.BaseName + ".stl";
		openscad $scadFile -o "./build/stl/$outFileName";
	}
}

if ($stlFiles.Count -gt 0) {
	Write-Host "Copying STL files...";

	foreach ($stlFile in $stlFiles) {
		Write-Host "`t" $stlFile.Name;
		$outFileName = $stlFile.BaseName + ".stl";
		Copy-Item -Path $stlFile -Destination "./build/stl/$outFileName";
	}
}

Write-Host "Repairing STL files...";

$stlFiles = Get-ChildItem -Path ./build/stl/*.stl;

foreach ($stlFile in $stlFiles) {
	$outFileName = $stlFile.BaseName + ".stl";
	~/Downloads/Slic3r-1.3.0-dev-a28f431-x86_64.AppImage --repair --no-gui $stlFile;
}

Write-Host "Slicing OBJ files...";

$repairedStlFiles = Get-ChildItem -Path ./build/stl/*.obj;

foreach ($stlFile in $repairedStlFiles) {
	$outFileName = $stlFile.BaseName + ".gcode";
	~/Downloads/Slic3r-1.3.0-dev-a28f431-x86_64.AppImage --no-gui $stlFile --output "./build/gcode/$outFileName" --load "./config/slic3r/config.ini";
}