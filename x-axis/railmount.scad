include <../common/core.scad>;
include <../common/shapes.scad>;

translate([0, 10, 6]) rotate([0, -90, 270]) railMount();
screwMount();

module railMount()
{
	linear_extrude(46) difference()
	{
		union()
		{
			square([9, 18]);
			translate([9, 9]) circle(r=9);
		}

		translate([9, 9]) circle(r=7.5);
		translate([9, 8]) square([20, 2]);
	}
}

module screwMount()
{
	width = 18;
	height = 66;
	edgeOffset = 5;
	
	difference()
	{
		linear_extrude(6) difference()
		{
			square([width, height]);
			translate([width/2, height-edgeOffset]) circle(r=1.5);
			translate([width/2, edgeOffset]) circle(r=1.5);
		}
		
		translate([0, 0, 3]) linear_extrude(5)
		{
			translate([width/2, height-edgeOffset]) hexagon2d(6.5);
			translate([width/2, edgeOffset]) hexagon2d(6.5);
		}
	}
}
