include <../common/core.scad>;
include <../common/parts.scad>;

linear_extrude(5) difference()
{
	union()
	{
		circle(d=25);
		translate([-12.5, 0]) square([25, 15]);
	}
	
	circle(d=19);
}

translate([-12.5, 20]) difference()
{
	rotate([90, 0, 0]) linear_extrude(5) difference()
	{
		square([35, 40]);
		translate([-1, 8]) square([25, 60]);
	}

	translate([30, -6, 0]) rotate([-90, 0, 0])
	{
		translate([0, -25, 0]) screw_m3(length=50);
		translate([0, -35, 0]) screw_m3(length=50);
	}
}
