include <../common/core.scad>;
include <../common/parts.scad>;
include <../common/shapes.scad>;

width = 13;
height = 66;
edgeOffset = 5;

screwMount();
translate([0, 10, 6]) beltMount();

module beltMount()
{
	teethHeight = height-20;
	linear_extrude(6) square([width, teethHeight]);

	translate([0, 0, 6]) linear_extrude(6)
	{
		square([width / 3, teethHeight]);
		translate([width /3 + 2.5, 0]) square([width / 3 * 2 - 2.5, teethHeight]);

		for (i=[0:teethHeight/2-1])
		{
			translate([4, i*2]) equilateralTriangle(2);
		}
	}
}

module screwMount()
{
	difference()
	{
		linear_extrude(6) difference()
		{
			square([width, height]);
			translate([width/2, height-edgeOffset]) circle(r=1.5);
			translate([width/2, edgeOffset]) circle(r=1.5);
		}
		
		translate([0, 0, 3]) linear_extrude(5)
		{
			translate([width/2, height-edgeOffset]) hexagon2d(6.5);
			translate([width/2, edgeOffset]) hexagon2d(6.5);
		}
	}
}
