include <../common/core.scad>;
include <../common/parts.scad>;
include <../common/shapes.scad>;

union()
{
	difference()
	{
		linear_extrude(4) union()
		{
			square([8, 46]);
			translate([-30, 0]) square([30, 8]);
			translate([0, 38]) square([35, 8]);
		}

		translate([-20, 4, 5]) rotate([0, 180, 0])
		{
			screw_m3(50);
			translate([-43, 38, 0]) screw_m3(50);
		}
	}

	translate([0, 43, 16]) rotate([0, 90, 0]) difference()
	{
		linear_extrude(4) square([16, 30]);

		translate([4, 6, 5]) rotate([0, 180, 0])
		{
			screw_m3(50);
			translate([0, 18, 0]) screw_m3(50);
		}
	}
}

