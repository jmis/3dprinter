include <../common/core.scad>;
include <../common/parts.scad>;

$width = 34;
$depth = 14;
$backHeight = 25;

translate([0, 0, 19]) rotate([-90, 0, 0]) difference()
{
    union()
    {
        linear_extrude(3.6) difference()
        {
            square([$width, $depth]);
            translate([$width/2, 0]) circle(d=16);
        }

        translate([0, 0, 3.6]) linear_extrude(5) difference()
        {
            square([$width, $depth]);
            translate([$width/2, 0]) circle(d=12);
        }
        
        translate([0, 0, 3.6 + 5]) linear_extrude(3.6) difference()
        {
            square([$width, $depth]);
            translate([$width/2, 0]) circle(d=16);
        }

        translate([0, $depth, 0]) linear_extrude($backHeight) square([$width, 5]);
    }

    translate([0, -5, 13.2/2]) rotate([-90, 0, 0])
    {
        translate([5, 0, 0]) screw_m3(length=50);
        translate([$width-5, 0, 0]) screw_m3(length=50);
    }

    translate([0, $depth-.2, $backHeight-5]) rotate([-90, 0, 0])
    {
         translate([5, 0, 0]) screw_m3(length=50);
         translate([$width-5, 0, 0]) screw_m3(length=50);
    }
}
