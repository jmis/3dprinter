include <../common/core.scad>;
include <../common/parts.scad>;

$width = 66;
$height = 90;

difference()
{
	linear_extrude(5) square([$width, $height]);

	// Top rail
	translate([0, $height-9, 5.2]) rotate([0, 180, 0])
	{
		translate([-5, 0, 0]) screw_m3(50);
		translate([-$width+5, 0, 0])screw_m3(50);
	}

	// Bottom rail
	translate([0, $height-46.5-9, 5.2]) rotate([0, 180, 0])
	{
		translate([-5, 0, 0]) screw_m3(50);
		translate([-$width+5, 0, 0])screw_m3(50);
	}

	// Extruder
	translate([$width/2-34/2, 20, 10]) rotate([0, 180, 0])
    {
         translate([-5, 0, 0]) screw_m3(length=50);
         translate([-34+5, 0, 0]) screw_m3(length=50);
    }

	// Wire holder
	translate([$width/2-34/2, 70, 10]) rotate([0, 180, 0])
    {
         translate([-5, 0, 0]) screw_m3(length=50);
         translate([-34+5, 0, 0]) screw_m3(length=50);
    }

	// Sensor
	translate([5, 0, 10]) rotate([0, 180, 0])
	{
		translate([0, 5, 0]) screw_m3(length=50);
		translate([0, 15, 0]) screw_m3(length=50);
	}

	// Belt holder
	translate([0, $height-46.5+9, 5.2]) rotate([0, 180, 0])
	{
		translate([-5, 0, 0]) screw_m3(50);
		translate([-$width+5, 0, 0])screw_m3(50);
	}
}
