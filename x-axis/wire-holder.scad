include <../common/core.scad>;
include <../common/parts.scad>;

$width = 66;
$depth = 14;
$backHeight = 20;

difference()
{
	union()
	{
		linear_extrude(4) difference()
		{
			translate([0, -3]) square([$width, 18]);
			translate([$width/2, 5]) circle(d=18);
			translate([$width/2 + 40 / 2, 5]) circle(d=18);
			translate([$width/2 - 20, 5]) circle(d=18);
		}

		translate([0, $depth, 0])
			linear_extrude($backHeight)
				square([$width, 5]);
	}

	translate([$width / 2 - 24 / 2, $depth-.2, $backHeight-5])
		rotate([270, 0, 0])
		{
			translate([0, 0, 0]) screw_m3(length=50);
			translate([24, 0, 0]) screw_m3(length=50);
		}
}
