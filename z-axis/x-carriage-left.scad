$fn=60;

use<MCAD/nuts_and_bolts.scad>;
use<MCAD/boxes.scad>;

$fn = 60;

carriageHeight = 62;
carriageDepth = 41.5;

guideRailVerticalSpacing = 46.5;
guideRailThickness = 8;

plateDistanceFromEdgeOfGuideRail = 0;

linearBearingOuterDiameter = 15;
linearBearingMountWallThickness = 2;
linearBearingMountDiameter = linearBearingOuterDiameter + linearBearingMountWallThickness;
linearBearingMountBlockSize = plateDistanceFromEdgeOfGuideRail + guideRailThickness / 2;

leadScrewFlangeDiameter = 22;
leadScrewFlangeBlockSize = leadScrewFlangeDiameter + 2;
leadScrewFlangeBlockThickness = 10;
leadScrewHoleDiameter = 10;

m3ScrewHeadHeight = 3.3;

translate([0, 0, carriageHeight]) rotate([0, 180, 0]) carriage();

module carriage()
{
	rail_block();
	translate([-leadScrewFlangeDiameter / 2, leadScrewFlangeBlockSize / 2, carriageHeight - leadScrewFlangeBlockThickness]) lead_screw_flange_mount();
	translate([-leadScrewFlangeDiameter / 2, leadScrewFlangeBlockSize / 2 + 21, 0]) single_rail_linear_bearing_mount();
	translate([0, -27, carriageHeight]) rotate([270, 0, 90]) end_stop_mount();
	translate([0, 41.5 * 2, carriageHeight]) rotate([90, 180, 90]) motor_mounting_plate();
}

module end_stop_mount()
{
    difference()
    {
        cube([27, 27, 5]);
        translate([0, 27, 5]) translate([3, -4, -10]) cylinder(d=3.3, h=30);
        translate([0, 27, 5]) translate([3, -22, -10]) cylinder(d=3.3, h=30);
        translate([18, 10, -1]) cube([15, 20, 7]);
    }
}

module rail_block()
{
    difference()
    {
        cube([18, carriageDepth, carriageHeight]);
        translate([3.5, -1, 15.5]) cube([11, 100, 31]);

        railZOffset = (carriageHeight - guideRailVerticalSpacing) / 2;

        translate([9, 50, railZOffset]) rotate([90, 0, 0]) cylinder(d=guideRailThickness + .5, h=100);
        translate([9, 50, carriageHeight - railZOffset]) rotate([90, 0, 0]) cylinder(d=guideRailThickness + .5, h=100);
    }
}

module lead_screw_flange_mount()
{
    difference()
    {
        translate([-leadScrewFlangeBlockSize / 2, -leadScrewFlangeBlockSize / 2, 0])
        cube([
            leadScrewFlangeBlockSize,
            leadScrewFlangeBlockSize,
            leadScrewFlangeBlockThickness
        ]);

        centerOfLeadScrew = [0, 0, 0];
        flip = [180, 0, 0];

        translate(centerOfLeadScrew) translate([0, 0, -1]) cylinder(d=leadScrewHoleDiameter, h=50);
        translate(centerOfLeadScrew) translate([0, 8, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1]) rotate(flip) screw_m3(20);
        translate(centerOfLeadScrew) translate([8, 0, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1]) rotate(flip) screw_m3(20);
        translate(centerOfLeadScrew) translate([-8, 0, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1]) rotate(flip) screw_m3(20);
        translate(centerOfLeadScrew) translate([0, -8, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1]) rotate(flip) screw_m3(20);
    }
}

module single_rail_linear_bearing_mount()
{
    difference()
    {
        cylinder(d=linearBearingOuterDiameter + linearBearingMountWallThickness, h=carriageHeight);
        translate([0, 0, -1]) cylinder(d=linearBearingOuterDiameter, h=carriageHeight+2);
        translate([-linearBearingOuterDiameter/2 - 2, -1, -1]) cube([5, 2, carriageHeight * 2]);
    }

    difference()
    {
        translate([0, -linearBearingMountDiameter / 2, 0])
        cube([leadScrewFlangeBlockSize / 2, linearBearingMountDiameter, carriageHeight]);
        translate([0, 0, -1]) cylinder(d=linearBearingOuterDiameter, h=carriageHeight+2);
    }
}

module motor_mounting_plate()
{
    difference()
    {
        cube([42, carriageHeight - 10, 18]);
        translate([21, carriageHeight / 2, 0]) motor_mounting_plate_holes();
    }
}

module motor_mounting_plate_holes()
{
	translate([-22, -10, 3.5]) cube([50, 20, 11]);
	translate([-22, -10, -1]) cube([29, 50, 50]);
	translate([0, 0, -1]) cylinder(d=25, h=50);
	translate([5.5 - 21, 5.5 -21, 25]) rotate([180, 0, 0]) screw_m3(length=30);
	translate([15, 5.5 - 21, 25]) rotate([180, 0, 0]) screw_m3(length=30);
	translate([5.5 - 21, 15, 25]) rotate([180, 0, 0]) screw_m3(length=30);
	translate([15, 15, 25]) rotate([180, 0, 0]) screw_m3(length=30);
}

module screw_m3(length=8, diameter=3)
{
    cylinder(d=5.5, h=3.3);
    translate([0, 0, 3.2]) cylinder(d=diameter + .1, h=length);
}
