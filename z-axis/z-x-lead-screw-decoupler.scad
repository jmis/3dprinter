include <../common/core.scad>;
include <../common/parts.scad>;
include <../common/shapes.scad>;

rotate([180, 0, 0]) difference()
{
	linear_extrude(10) union()
	{
		difference()
		{
			translate([14, 0]) hull()
			{
				translate([0, 14]) circle(14);
				translate([27, 14]) circle(14);
				translate([0, 23]) circle(14);
			}

			translate([33, 14 - 4.5]) hull()
			{
				translate([0, 5]) circle(4.5);
				square([40, 10]);
			}
		}
	}

	translate([14, 14, -0.5]) union()
	{
		linear_extrude(4) circle(11.5);
		linear_extrude(30) circle(5.5);
	}
}
