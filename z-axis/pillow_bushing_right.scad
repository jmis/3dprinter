include <MCAD/boxes.scad>;

$fn=60;

full_pillow_bushing();

module full_pillow_bushing()
{
    pillow_bushing_plate();
    translate([0, -8, 0]) wall_mounting_plate();
}

module pillow_bushing_plate()
{
    difference()
    {
        pillow_bushing_plate_without_holes();       
        translate([21, 33, -1]) cylinder(d=22.75, h=50);
        translate([0, 33, -1]) cylinder(d=8.5, h=10);
    }
}

module pillow_bushing_plate_without_holes()
{
    cube([42, 54, 8]);
    translate([0, 33, 0]) cylinder(d=15, h=8);
}

module wall_mounting_plate()
{
    difference()
    {
        cube([42, 8, 60]);
        translate([42 / 2, 0, 60 / 3 * 2.2]) rotate([90, 0, 0]) roundedBox([42 - 42 * .2 - 2, 4.1, 10], 1, true);
		translate([42 / 2, 9, 60 / 3 * 2.2]) rotate([90, 0, 0]) roundedBox([42 - 42 * .2 + 2, 8, 10], 4, true);

		//translate([42 / 2, 0, 60  / 3 * 2.2]) rotate([90, 0, 0]) roundedBox([42 - 42 * .2 - 2, 4.1, 10], 1, true);
		//translate([42 / 2, 9, 60 / 3 * 2.2]) rotate([90, 0, 0]) roundedBox([42 - 42 * .2 + 2, 8, 10], 4, true);
        
        //translate([12, 8.1, 15]) rotate([90, 0, 0]) pan_head_screw();
        //translate([21, 8.1, 30]) rotate([90, 0, 0]) pan_head_screw();
    }
}



module pan_head_screw()
{
    cylinder(d=8, h=4.5);
    translate([0, 0, 3.2]) cylinder(d=4 + .1, h=10);
}