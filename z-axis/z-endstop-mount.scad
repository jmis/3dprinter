$fn=60;

end_stop_mount();

module end_stop_mount()
{
    difference()
    {
        cube([27, 27, 8]);
        translate([0, 27, 5]) translate([3, -4, -10]) cylinder(d=3.3, h=30);
        translate([0, 27, 5]) translate([3, -22, -10]) cylinder(d=3.3, h=30);
        translate([19, 21, 8.1]) rotate([180, 0, 0]) pan_head_screw();
        translate([19, 5.5, 8.1]) rotate([180, 0, 0]) pan_head_screw();
    }
}

module pan_head_screw()
{
    cylinder(d=8, h=4.5);
    translate([0, 0, 3.2]) cylinder(d=4 + .1, h=30);
}
