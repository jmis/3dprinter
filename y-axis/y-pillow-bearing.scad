$fn=60;

full_pillow_bushing();

module full_pillow_bushing()
{
    pillow_bushing_plate();
    translate([0, -8, 0]) wall_mounting_plate();
}

module pillow_bushing_plate()
{
    difference()
    {
        pillow_bushing_plate_without_holes();       
        translate([21, 21, -1]) cylinder(d=22.75, h=50);
    }
}

module pillow_bushing_plate_without_holes()
{
    cube([42, 36, 5]);
}

module wall_mounting_plate()
{
    difference()
    {
        cube([42, 8, 40]);
        translate([12, 8.1, 15]) rotate([90, 0, 0]) pan_head_screw();
        translate([30, 8.1, 15]) rotate([90, 0, 0]) pan_head_screw();
        translate([21, 8.1, 30]) rotate([90, 0, 0]) pan_head_screw();
    }
}

module pan_head_screw()
{
    cylinder(d=8, h=4.5);
    translate([0, 0, 3.2]) cylinder(d=4 + .1, h=10);
}