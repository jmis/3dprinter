/*
   bearing OD - 22mm
   motor width and height - 42mm
   wall mount thickness -- 9mm
   guide rail diameter - 8mm
   guide rail distance from lead screw - 21mm
*/

$fn=60;
wall_mounting_plate();
translate([-11, 8, 0]) end_stop_mount();

module wall_mounting_plate()
{
    difference()
    {
        cube([25, 8, 25]);
        translate([12.5, 8.1, 12.5]) rotate([90, 0, 0]) pan_head_screw();
    }
}

module screw_m3(length=8, diameter=3.5)
{
    cylinder(d=5.5, h=3.3);
    translate([0, 0, 3.2]) cylinder(d=diameter + .1, h=length);
}

module pan_head_screw()
{
    cylinder(d=8, h=4.5);
    translate([0, 0, 3.2]) cylinder(d=4 + .1, h=10);
}

module end_stop_mount()
{
    difference()
    {
        cube([30, 55, 5]);
        translate([0, 55, 5]) translate([3, -4, -10]) cylinder(d=3.3, h=30);
        translate([0, 55, 5]) translate([3, -22, -10]) cylinder(d=3.3, h=30);
		translate([-1, -1, -1]) cube([19, 29, 10]);
		color([1, 0, 0]) translate([0, 0, -1]) translate([8, 25, -1]) cube([10, 20, 10]);
    }
}