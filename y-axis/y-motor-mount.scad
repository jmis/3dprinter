/*
   bearing OD - 22mm
   motor width and height - 42mm
   wall mount thickness -- 9mm
   guide rail diameter - 8mm
   guide rail distance from lead screw - 21mm
*/

$fn=60;
full_motor_mount();

module full_motor_mount()
{
    translate([0, -8, 0]) wall_mounting_plate();
    motor_mounting_plate();
}

module motor_mounting_plate()
{
    difference()
    {
        motor_mounting_plate_without_holes();
        
        translate([21, 21, -1]) cylinder(d=25, h=50);
        translate([5.5, 5.5, 10]) rotate([180, 0, 0]) screw_m3();
        translate([36, 5.5, 10]) rotate([180, 0, 0]) screw_m3();
        translate([5.5, 36, 10]) rotate([180, 0, 0]) screw_m3();
        translate([36, 36, 10]) rotate([180, 0, 0]) screw_m3();
    }
}

module motor_mounting_plate_without_holes()
{
    cube([42, 42, 5]);
}

module wall_mounting_plate()
{
    difference()
    {
        cube([42, 8, 40]);
        translate([12, 8.1, 15]) rotate([90, 0, 0]) pan_head_screw();
        translate([30, 8.1, 15]) rotate([90, 0, 0]) pan_head_screw();
        translate([21, 8.1, 30]) rotate([90, 0, 0]) pan_head_screw();
    }
}

module screw_m3(length=8, diameter=3.5)
{
    cylinder(d=5.5, h=3.3);
    translate([0, 0, 3.2]) cylinder(d=diameter + .1, h=length);
}

module pan_head_screw()
{
    cylinder(d=8, h=4.5);
    translate([0, 0, 3.2]) cylinder(d=4 + .1, h=10);
}