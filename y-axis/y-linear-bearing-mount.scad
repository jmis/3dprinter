use<MCAD/nuts_and_bolts.scad>;
use<MCAD/boxes.scad>;

$fn=60;

difference()
{
    cube([25, 27, 35]);
    translate([12.5, 35, 20.5]) rotate([90, 0, 0]) cylinder(d=15.3, h=40, center=false);
    translate([6, -5, 20.5]) cube([13, 40, 20]);
    translate([23, 13.5, 30]) rotate([0, 270, 0]) boltHole(size=4, length=30, $fn=60);
	
	translate([12, 14, 15]) rotate([180, 0, 0]) pan_head_screw();
}

module pan_head_screw()
{
    cylinder(d=8, h=4.5);
    translate([0, 0, 3.2]) cylinder(d=4 + .1, h=20);
}