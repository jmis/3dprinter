use<MCAD/nuts_and_bolts.scad>;
use<MCAD/boxes.scad>;

$fn = 60;

carriageHeight = 70;

guideRailVerticalSpacing = 46.5;
guideRailThickness = 8;

plateDistanceFromEdgeOfGuideRail = 8;

linearBearingOuterDiameter = 15;
linearBearingMountWallThickness = 1.5;
linearBearingMountDiameter = linearBearingOuterDiameter + linearBearingMountWallThickness;
linearBearingMountBlockSize = plateDistanceFromEdgeOfGuideRail + guideRailThickness / 2;

leadScrewFlangeDiameter = 22;
leadScrewFlangeBlockSize = leadScrewFlangeDiameter + 2;
leadScrewFlangeBlockThickness = 10;
leadScrewHoleDiameter = 10;

m3ScrewHeadHeight = 3.3;

/*
    6mm on top and bottom
    10mm center
    16mm between center of holes
*/

difference()
{
    translate([0, 35 - linearBearingMountWallThickness / 2, 0])
    y_axis_carriage();
    
    translate([0, 15, 15])
    rotate([0, 90, 0])
    screw_m3(30);

    translate([0, 15, 55])
    rotate([0, 90, 0])
    screw_m3(30);

    translate([0, linearBearingOuterDiameter + 70 + guideRailVerticalSpacing - 15, 15])
    rotate([0, 90, 0])
    screw_m3(30);

    translate([0, linearBearingOuterDiameter + 70 + guideRailVerticalSpacing - 15, 55])
    rotate([0, 90, 0])
    screw_m3(30);
}

module y_axis_carriage()
{
    dual_rail_linear_bearing_mounts();
    mounting_plate();
    
    translate([0, (linearBearingMountDiameter / 2) + (guideRailVerticalSpacing / 2) - leadScrewFlangeBlockSize / 2, 0])
    lead_screw_flange_mount();
}

module lead_screw_flange_mount()
{
    difference()
    {
        translate([-plateDistanceFromEdgeOfGuideRail / 2, 0, 0])
            cube([
                leadScrewFlangeBlockSize + plateDistanceFromEdgeOfGuideRail,
                leadScrewFlangeBlockSize,
                leadScrewFlangeBlockThickness
            ]);
        
        color([1, 0, 0])
        translate(center_of_lead_screw())
        translate([0, 0, -1])
        cylinder(d=leadScrewHoleDiameter, h=50);
        
        color([0, 0, 1])
        translate(center_of_lead_screw())
        translate([0, 8, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1])
        rotate([180, 0, 0])
        screw_m3(20);
        
        color([0, 0, 1])
        translate(center_of_lead_screw())
        translate([8, 0, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1])
        rotate([180, 0, 0])
        screw_m3(20);
        
        color([0, 0, 1])
        translate(center_of_lead_screw())
        translate([-8, 0, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1])
        rotate([180, 0, 0])
        screw_m3(20);
        
        color([0, 0, 1])
        translate(center_of_lead_screw())
        translate([0, -8, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1])
        rotate([180, 0, 0])
        screw_m3(20);
    }
}

function center_of_lead_screw() =
    [leadScrewFlangeBlockSize / 2 - plateDistanceFromEdgeOfGuideRail + 4, leadScrewFlangeBlockSize / 2, 0];

module mounting_plate()
{
    difference()
    {
        translate([
            plateDistanceFromEdgeOfGuideRail + guideRailThickness/2 + (linearBearingMountDiameter/ 2),
            linearBearingMountWallThickness / 2 - 35,
            0
        ])
        cube([
            8,
            linearBearingOuterDiameter + 70 + guideRailVerticalSpacing,
            carriageHeight
        ]);
    }
}

module dual_rail_linear_bearing_mounts()
{
    single_rail_linear_bearing_mount();
    translate([0, guideRailVerticalSpacing, 0]) single_rail_linear_bearing_mount();
}

module single_rail_linear_bearing_mount()
{
    difference()
    {
        translate([linearBearingMountDiameter/2, linearBearingMountDiameter/2, 0])
        cylinder(d=linearBearingOuterDiameter + linearBearingMountWallThickness, h=carriageHeight);
        
        translate([linearBearingMountDiameter/2, linearBearingMountDiameter/2, -1])
        cylinder(d=linearBearingOuterDiameter, h=carriageHeight+2);
        
        translate([-2, linearBearingOuterDiameter/2, -1])
        cube([5, 2, carriageHeight * 2]);
    }
    
    difference()
    {
        translate([linearBearingMountDiameter - linearBearingMountDiameter / 2, (linearBearingMountDiameter - linearBearingMountDiameter / 2) - linearBearingOuterDiameter / 2, 0])
        cube([linearBearingMountBlockSize, linearBearingOuterDiameter, carriageHeight]);
        
        translate([linearBearingMountDiameter/2, linearBearingMountDiameter/2, -1])
        cylinder(d=linearBearingMountDiameter-1, h=carriageHeight+2);    
    }
}

module screw_m3(length=8, diameter=3)
{
    cylinder(d=5.5, h=3.3);
    translate([0, 0, 3.2]) cylinder(d=diameter + .1, h=length);
}
