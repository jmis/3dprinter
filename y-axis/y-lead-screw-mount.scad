$fn = 60;

leadScrewFlangeDiameter = 22;
leadScrewFlangeBlockSize = leadScrewFlangeDiameter + 2;
leadScrewFlangeBlockThickness = 10;
leadScrewHoleDiameter = 10;

m3ScrewHeadHeight = 3.3;

color([1, 0, 0]) lead_screw_flange_mount();
color([0, 1, 0]) translate([21, 8 + leadScrewFlangeBlockSize / 2, 0]) rotate([180, 180, 0]) wall_mounting_plate();

module lead_screw_flange_mount()
{
    difference()
    {
        translate([-leadScrewFlangeBlockSize / 2, -leadScrewFlangeBlockSize / 2, 0])
        cube([
            leadScrewFlangeBlockSize,
            leadScrewFlangeBlockSize,
            leadScrewFlangeBlockThickness
        ]);

        centerOfLeadScrew = [0, 0, 0];
        flip = [180, 0, 0];

        translate(centerOfLeadScrew) translate([0, 0, -1]) cylinder(d=leadScrewHoleDiameter, h=50);
        translate(centerOfLeadScrew) translate([0, 8, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1]) rotate(flip) screw_m3(20);
        translate(centerOfLeadScrew) translate([8, 0, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1]) rotate(flip) screw_m3(20);
        translate(centerOfLeadScrew) translate([-8, 0, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1]) rotate(flip) screw_m3(20);
        translate(centerOfLeadScrew) translate([0, -8, leadScrewFlangeBlockThickness + m3ScrewHeadHeight + .1]) rotate(flip) screw_m3(20);
    }
}

module screw_m3(length=8, diameter=3)
{
    cylinder(d=5.5, h=3.3);
    translate([0, 0, 3.2]) cylinder(d=diameter + .1, h=length);
}

module wall_mounting_plate()
{
    difference()
    {
        cube([42, 8, 40]);
        translate([12, 8.1, 15]) rotate([90, 0, 0]) pan_head_screw();
        translate([30, 8.1, 15]) rotate([90, 0, 0]) pan_head_screw();
        translate([21, 8.1, 30]) rotate([90, 0, 0]) pan_head_screw();
    }
}

module pan_head_screw()
{
    cylinder(d=8, h=4.5);
    translate([0, 0, 3.2]) cylinder(d=4 + .1, h=10);
}