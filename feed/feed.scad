include <MCAD/boxes.scad>;
include <../common/core.scad>;
include <../common/parts.scad>;

motor_mounting_plate();

module motor_mounting_plate()
{
	difference()
	{
		motor_mounting_plate_without_holes();
		translate([21, 21, -1]) motor_mounting_plate_holes();
	}
}

module motor_mounting_plate_without_holes()
{
	translate([0, 17, 5]) cube([42, 25, 15]);
	translate([5.5, 5.5, 7]) cylinder(d=6, h=10);
	translate([5.5, 5.5, 5]) cylinder(d=10, h=2);
	translate([0, 0, 0]) cube([42, 42, 5]);
}

module motor_mounting_plate_holes()
{
	translate([0, 0, 0]) cylinder(d=25, h=6.1);
	translate([0, 0, -1]) cylinder(d=22, h=30.1);
	translate([5.5 - 21, 5.5 -21, 29.5]) rotate([180, 0, 0]) screw_m3(length=30);
	translate([15, 5.5 - 21, 19.5]) rotate([180, 0, 0]) screw_m3(length=30);
	translate([5.5 - 21, 15, 19.5]) rotate([180, 0, 0]) screw_m3(length=30);
	translate([15, 15, 19.5]) rotate([180, 0, 0]) screw_m3(length=30);

	translate([-6, 22, 15]) rotate([90, 90, 0]) cylinder(d=2.2, h=50);
	translate([-6, 22, 15]) rotate([90, 90, 0]) cylinder(d=5, h=6.1);
	
	translate([21 - 5.5, -2, 15]) rotate([90, 90, 0]) cylinder(d=9, h=50);
	
	translate([-22, -10, 6]) cube([10, 40, 30]);
	translate([-22, 5, 6]) cube([20, 5, 30]);
	translate([21-10, 21-10, 6]) cube([20, 20, 30]);
	translate([-21, -10, 6]) cube([15, 17, 30]);
}
