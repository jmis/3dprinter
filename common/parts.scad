include <core.scad>;

module screw_m3(length=8, diameter=3)
{
	cylinder(d=5.5, h=3.3);
	translate([0, 0, 3.2]) cylinder(d=diameter + .1, h=length);
}

module pan_head_screw()
{
	cylinder(d=8, h=4.5);
	translate([0, 0, 3.2]) cylinder(d=4 + .1, h=10);
}