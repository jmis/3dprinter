include <core.scad>;

module hexagon2d(diameter)
{
	intersection()
	{
		square([diameter,2*diameter],center=true);
		rotate(60) square([diameter,2*diameter],center=true);
		rotate(-60) square([diameter,2*diameter],center=true);
	}
}

module equilateralTriangle(size)
{
	polygon([[0, 0], [0, size], [size, size/2], [0, 0]]);
}

module roundedSquare(dimensions, radius)
{
	translate([0, radius]) square([dimensions[0], dimensions[1] - radius * 2]);
	translate([radius, 0]) square([dimensions[0] - radius * 2, dimensions[1]]);
	translate([radius, radius]) circle(radius);
	translate([radius, dimensions[0] - radius]) circle(radius);
	translate([dimensions[0] - radius, radius]) circle(radius);
	translate([dimensions[0] - radius, dimensions[1] - radius]) circle(radius);
}
